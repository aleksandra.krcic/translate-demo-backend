import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import TranslateAllVersions = require('@google-cloud/translate');
const { Translate } = TranslateAllVersions.v2;

@Injectable()
export class TranslateService {
  translateApi;

  constructor() {
    dotenv.config();
    const key = process.env.KEY;
    this.translateApi = new Translate({ key });
  }

  async translateText(text: string, language: string): Promise<any> {
    const [translations] = await this.translateApi.translate(text, language);
    return { translation: translations };
  }
}
