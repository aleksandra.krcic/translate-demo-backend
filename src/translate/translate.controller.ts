import { Body, Controller, Post } from '@nestjs/common';
import { TextToTranslate } from 'src/dto/textToTranslate.dto';
import { TranslateService } from './translate.service';

@Controller('translate')
export class TranslateController {
  constructor(private readonly translateService: TranslateService) {}

  @Post()
  async translateText(@Body() textToTranslate: TextToTranslate) {
    return await this.translateService.translateText(
      textToTranslate.text,
      textToTranslate.language,
    );
  }
}
